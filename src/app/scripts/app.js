import $ from 'jquery';
// import tether from 'tether';
import 'bootstrap/js/src/collapse';
import 'bootstrap/js/src/alert';
import 'bootstrap/js/src/modal';
import 'bootstrap/js/src/dropdown';
import "bootstrap-validator/dist/validator.min";
// import "moment/moment";
// import "moment-timezone/builds/moment-timezone-with-data-2012-2022";
// import 'js-offcanvas/dist/_js/js-offcanvas';
// import "eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker";
// import "readmore-js/readmore";

/*!
 * IE10 viewport hack for Surface/desktop Windows 8 bug
 * Copyright 2014-2017 The Bootstrap Authors
 * Copyright 2014-2017 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
// See the Getting Started docs for more information:
// https://getbootstrap.com/getting-started/#support-ie10-width
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
  var msViewportStyle = document.createElement('style')
  msViewportStyle.appendChild(
    document.createTextNode(
      '@-ms-viewport{width:auto!important}'
    )
  )
  document.head.appendChild(msViewportStyle)
}

/*! @license credits */
// const $ = window.jQuery;

/**
 * Update copyright year to current year.
 */
const copyYear = document.getElementById('copy-year');
if (copyYear) {
  copyYear.innerHTML = new Date().getFullYear();
}

const _datetimepickerIcons = {
  next: 'icon icon-arrow_forward',
  previous: 'icon icon-arrow_back',
  up: 'icon icon-arrow_upward',
  down: 'icon icon-arrow_downward',
  date: 'icon icon-date_range',
  time: 'icon icon-schedule'
};

/**
 * Set moment locale globally
 */
if (window.language) {
  moment.locale(window.language);
}

/**
 * Get datetimepicker options
 * @see http://eonasdan.github.io/bootstrap-datetimepicker
 * @param  {String}  id
 * @param  {String} dateOrTime
 * @return {Object}
 */
function _getDatetimePickerOpts (id, dateOrTime) {
  let options = {
    debug: true,
    // keepInvalid: true,
    // keepOpen: true,
    ignoreReadonly: true,
    widgetParent: $('#group_' + id),
    toolbarPlacement: 'top',
    icons: _datetimepickerIcons
  };
  if (window.language) {
    options.locale = window.language;
  }

  if (dateOrTime === 'date') {
    options.dayViewHeaderFormat = 'MMMM YYYY';
    // const today = new Date();
    // let yesterday = new Date(today);
    // let yesterday.setDate(today.getDate() - 1);
    // options.minDate = window.mindate || yesterday; // @@hack datepicker yesterday
    options.minDate = window.mindate || new Date();
    if (window.maxdate) {
      options.maxDate = window.maxdate;
    }
    options.format = 'L';
  } else {
    options.format = 'LT'; // @see http://stackoverflow.com/a/31300407/1938970
    options.stepping = 10; // numberof minutes step in timepicker
    options.keepOpen = true;
  }

  // stretch both pickers to full width on small screen
  if (window.innerWidth < 600) {
    const $parentRow = $('#group_' + id).parents('.row').first();
    $parentRow.css('position', 'relative');
    options.widgetParent = $parentRow;
  }

  return options;
}

/**
 * Bootstrap datetimepicker (date)
 */
function initDatePickers () {
  const $dateOutbound = $('#date_outbound');
  const $dateReturn = $('#date_return');
  $dateOutbound
    .datetimepicker(_getDatetimePickerOpts('date_outbound', 'date'))
    .prev().click(() => {
      $dateOutbound.focus();
    });
  $dateReturn
    .datetimepicker(_getDatetimePickerOpts('date_return', 'date'))
    .prev().click(() => {
      $dateReturn.focus();
    });
}

/**
 * Bootstrap datetimepicker (time)
 */
function initTimePickers () {
  const $timeOutbound = $('#time_outbound');
  const $timeReturn = $('#time_return');

  $timeOutbound
    .datetimepicker(_getDatetimePickerOpts('time_outbound', 'time'))
    .prev().click(() => {
      $timeOutbound.focus();
    });
  $timeReturn
    .datetimepicker(_getDatetimePickerOpts('time_return', 'time'))
    .prev().click(() => {
      $timeReturn.focus();
    });
}

/**
 * Reinit datetimepickers, for responsiveness
 * (for now we only need to reinit the time pickers)
 */
function reinitDatetimepickers() {
  // const $dateOutbound = $('#date_outbound');
  // const $dateReturn = $('#date_return');
  const $timeOutbound = $('#time_outbound');
  const $timeReturn = $('#time_return');

  $timeOutbound.data('DateTimePicker').destroy();
  $timeReturn.data('DateTimePicker').destroy();

  initTimePickers();
}

/**
 * TrustPilot widget
 * grabbed from http://jsfiddle.net/pilch/GmttU/
 *
 * See trustpilot iframe widget options
 * @link(http://trustpilot.github.io/developers/#trustbox, here)
 */
function trustPilotWidget() {
  var $widgets = $('.trustpilot-widget');
  if (!$widgets.length) {
    return;
  }
  var a = document.location.protocol === 'https:' ? 'https://ssl.trustpilot.com' : 'http://s.trustpilot.com',
    b = document.createElement('script');
  b.type = 'text/javascript';
  b.async = true;
  b.src = a + '/tpelements/tp_elements_all.js';
  var c = document.getElementsByTagName('script')[0];
  c.parentNode.insertBefore(b, c);
}

/**
 * Mobile off canvas menu
 *
 * @see https://www.npmjs.com/package/js-offcanvas
 */
function mobileMenu () {
  $(document).trigger('enhance');
}

/**
 * Form validation
 * @link(http://1000hz.github.io/bootstrap-validator/)
 */
function formValidation () {
  $('.form-validate').validator({
    feedback: {
      // success: 'icon icon-check',
      // error: 'icon icon-warning' // icon icon-cross
    }
  });
}

/**
 * Initialize readmore areas,
 * using readmore.js jQuery plugin
 *
 * @link(http://jedfoster.com/Readmore.js/)
 */
function initReadmore() {
  var $readmoreAreas = $('.readmore');

  $readmoreAreas.readmore({
    speed: 180,
    moreLink: '<div class="readmore-toggle"><a href="#">Read more</a> <i class="icon icon-arrow_drop_down"></i></div>',
    lessLink: '<div class="readmore-toggle"><a href="#">Close</a> <i class="icon icon-arrow_drop_up"></i></div>',
    afterToggle: function (trigger, element, expanded) {
      // console.log('ciao');
      $(element).toggleClass('expanded', expanded);
      // return expanded;
    }
  });
}

let dragEnd;
/**
 * Init carousels
 * @uses dragend
 * @see https://github.com/Stereobit/dragend
 */
function reinitCarousels () {
  const carousel = document.querySelector('.carousel');
  if (!carousel) {
    return;
  }
  const winWidth = window.innerWidth;
  let itemsInPage = 1;

  if (winWidth < 992) {
    if (winWidth > 768) {
      itemsInPage = 3;
    }
    else if (winWidth > 575) {
      itemsInPage = 2;
    }

    if (dragEnd) {
      dragEnd.destroy();
    }
    dragEnd = new Dragend(carousel, {
      itemsInPage: itemsInPage,
      scribe: '50px',
    });
  } else {
    if (dragEnd) {
      dragEnd.destroy();
    }
  }
}

/**
 * Init UI
 */
function initUI () {
  mobileMenu();
  initDatePickers();
  // initTimePickers();
  trustPilotWidget();
  formValidation();
  initReadmore();
  reinitCarousels();

  // $(window).resize($.debounce(250, reinitDatetimepickers));
  $(window).resize($.debounce(250, reinitCarousels));
}

// init UI on ready
$(document).ready(initUI);

window.t2a = window.t2a || {};

window.t2a['initTimePickers'] = initTimePickers;
window.t2a['reinitDatetimepickers'] = reinitDatetimepickers;
